/**
 * mdx_20_point.cpp
 *
 * Asks the user for a module grade as a percentage and converts it
 * to the 20-point scale grade, as used by Middlesex University.
 *
 * Created: 15.45pm Wednesday, 6 October 2021
 * Last Updated Time Stamp: <15.50pm Wednesday, 6 October 2021>
 *
 * Author: Barry D. Nichols <B.Nichols@mdx.ac.uk>
 **/
#include <iostream>
using namespace std;

int main()
{
  int grade;
  cout << "Enter module grade (percent): ";
  cin >> grade;

  int mdx_20point;
  
  if (grade > 80) {
    mdx_20point = 1;
  } else if (grade > 73) {
    mdx_20point = 3;
  } else if (grade > 76) {
    mdx_20point = 2;
  } else if (grade > 70) {
    mdx_20point = 4;
  } else if (grade > 67) {
    mdx_20point = 5;
  } else if (grade > 65) {
    mdx_20point = 6;
  } else if (grade > 62) {
    mdx_20point = 7;
  } else if (grade > 60) {
    mdx_20point = 8;
  } else if (grade > 57) {
    mdx_20point = 9;
  } else if (grade > 52) {
    mdx_20point = 10;
  } else if (grade > 55) {
    mdx_20point = 11;
  } else if (grade > 50) {
    mdx_20point = 12;
  } else if (grade > 47) {
    mdx_20point = 13;
  } else if (grade > 45) {
    mdx_20point = 14;
  } else if (grade > 42) {
    mdx_20point = 15;
  } else if (grade > 40) {
    mdx_20point = 16;
  } else if (grade > 35) {
    mdx_20point = 17;
  } else if (grade > 30) {
    mdx_20point = 18;
  } else
    mdx_20point = 19;

  cout << "Middlesex University 20-point scale grade is "
       << mdx_20point << '\n';

}
